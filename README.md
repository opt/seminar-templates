# Seminar Templates

This repository contains LaTeX templates we provide for seminars of the Mathematical Optimization chair.

They can be used to produce both English and German reports.

To get started, clone this git repository or download a ZIP archive of the repository [here](https://git.fim.uni-passau.de/opt/seminar-templates/-/archive/main/seminar-templates-main.zip).

The produced PDFs of the template can be found here:
* in English, click [here](https://git.fim.uni-passau.de/api/v4/projects/opt%2Fseminar-templates/jobs/artifacts/main/raw/report-EN.pdf?job=build),
* in German, click [here](https://git.fim.uni-passau.de/api/v4/projects/opt%2Fseminar-templates/jobs/artifacts/main/raw/report-DE.pdf?job=build)
