\section{Creating Bibliographies with Bib\TeX}

In academic work, it is important to place great value on a correct bibliography. With Bib\TeX, beautiful and norm-compliant bibliographies can be created. In addition, this tool offers the possibility to collect all books or works that one has used during one's academic career in a "virtual library". If you want to write a paper with \LaTeX \ in the future and cite one of the collected works, you can do so easily and without much effort.

\subsection*{References}

All essential information about the literature used is stored in a file with the extension \texttt{.bib}. These files contain an entry for each specified work, which has corresponding attributes depending on the type of reference.

Here is an example of an entry for a (very good :-) ) book on network flow problems:

{\small\begin{verbatim}
@book{ahuja:93,
  author    = {Ahuja, Ravindra K. and Magnanti, Thomas L. and Orlin, James B.},
  title     = {Network Flows~: {Theory, Algorithms, and Applications}},
  publisher = {Prentice Hall},
  address   = {Englewood Cliffs, New Jersey},
  ISBN      = {013617549X},
  year      = {1993}
}
\end{verbatim}}

The following types of references are available:

\begin{table}[H]
    \centering
        \begin{tabular}[t]{|l|l|}
        \hline
        @book           & A book published by a publisher.\\
        \hline
        @booklet        & Printed work without an explicit publisher.\\
        \hline
        @article        & An article published in a magazine or journal.\\
        \hline
        @incollection   & A part of a book with its own title.\\
        \hline
        @manual         & A technical documentation.\\
        \hline
        @mastersthesis  & Master's thesis\\
        \hline
        @phdthesis      & Doctoral thesis\\
        \hline
        @proceedings    & Course or collection proceedings.\\
        \hline
        @techreport     & A report published by a school or similar institution\\ 
                        & (usually within a series of reports).\\
        \hline
        @unpublished    & A document with title and author but not formally published.\\
        \hline
        @misc           & A work that cannot be classified in any other category.\\
        \hline
        \end{tabular}
    \caption{Bib\TeX \ reference types}
\end{table}

Depending on the type of reference, some information about a work is required, optional, or not necessary. An overview of the most important attribute fields is given in the following table:

\begin{table}[H]
        \begin{tabular}{|l|l|}
            \hline
            author      & Name of the author or authors\\
            \hline
            booktitle   & Title of a book or part of a book. For reference to a\\
                        & whole book, the field \texttt{title} is available.\\
            \hline
            chapter     & A chapter number or chapter designation.\\
            \hline
            edition     & Edition of the book, can be a number or a spelled-out number.\\
            \hline
            institution & Institution where the work was created.\\
            \hline
            journal     & Name of the journal or magazine.\\
            \hline
            month       & Month of publication\\
            \hline
            pages       & One or more page numbers,\\
                        & e.g. 12 -- 30 or 23, 40, 57.\\
            \hline
            publisher   & Name of the publisher.\\
            \hline
            title       & Title of the work.\\
            \hline
            year        & Year of publication\\
            \hline
            ISBN        & International Standard Book Number\\
            \hline
            language    & Language in which the work is written.\\
            \hline
            URL         & Universal Resource Locator, specifying a web address\\
            \hline
        \end{tabular}
    \caption{Literature attribute fields}
\end{table}

\subsection*{Citing} 

After the actual information about the literature used has been stored in the .bib file, this information must be linked to the appropriate places in the text where the work is cited. For this purpose, the command \texttt{cite{}} (for citation) is used:

{\small\begin{verbatim}
Details on the implementation of the network simplex algorithm can be found in the book 
"Network Flows: Theory, Algorithms, and Applications" \cite{ahuja:93}.
\end{verbatim}}

The output looks, depending on the display style settings, something like this:

Details on the implementation of the network simplex algorithm can be found in the book "Network Flows: Theory, Algorithms, and Applications"  \cite{ahuja:93}.

\vspace{0.5em}
Further information on citing can be found in the citation guide of the Mathematics Student Council.

\subsection*{Including the Bibliography}

To create and include the actual bibliography, the seminar paper must be compiled twice. 
